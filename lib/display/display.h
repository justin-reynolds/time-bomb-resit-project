#include <avr/io.h>
#include <string.h>
#include <sound.h>

#define LOW 0
#define HIGH 1
/* Define shift register pins used for seven segment display */
#define LATCH_DIO PD4
#define CLK_DIO PD7
#define DATA_DIO PB0

#define LSBFIRST 0
#define MSBFIRST 1
#define NUMBER_OF_SEGMENTS 8

#define sbi(register, bit) (register |= _BV(bit))
#define cbi(register, bit) (register &= ~_BV(bit))

void initDisplay();
void writeNumberToSegment(uint8_t segment, uint8_t value);
void displayNumber(int countNum);

void writeAnimation1();
void writeValueToSegment(uint8_t segment, uint8_t value);
void writeAllValuesToDisplay(uint8_t value1,uint8_t value2,uint8_t value3,uint8_t value4);