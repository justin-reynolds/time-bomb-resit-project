#include "display.h"

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

#define SPACE 0xFF

/* Segment byte maps for numbers 0 to 9 */
const uint8_t SEGMENT_MAP[] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99,
                               0x92, 0x82, 0xF8, 0X80, 0X90};

/* Byte maps to select digit 1 to 4 */
const uint8_t SEGMENT_SELECT[] = {0xF1, 0xF2, 0xF4, 0xF8};

void initDisplay()
{
  sbi(DDRD, LATCH_DIO);
  sbi(DDRD, CLK_DIO);
  sbi(DDRB, DATA_DIO);
}

// loop through seven segments of LED display and shift the correct bits in the
// data register
void shift(uint8_t val, uint8_t bitorder)
{
  uint8_t bit;

  for (uint8_t i = 0; i < NUMBER_OF_SEGMENTS; i++)
  {
    if (bitorder == LSBFIRST)
    {
      bit = !!(val & (1 << i));
    }
    else
    {
      bit = !!(val & (1 << (7 - i)));
    }
    // write bit to register
    if (bit == HIGH)
      sbi(PORTB, DATA_DIO);
    else
      cbi(PORTB, DATA_DIO);

    // Trigger the clock pin so the display updates
    sbi(PORTD, CLK_DIO);
    cbi(PORTD, CLK_DIO);
  }
}

// Writes a digit to a certain segment. Segment 0 is the leftmost.
void writeNumberToSegment(uint8_t segment, uint8_t value)
{
  cbi(PORTD, LATCH_DIO);
  if (value == SPACE)
  {
    // Clear the segment
    shift(0xFF, MSBFIRST);
  }
  else
  {
    // Write the segment value
    shift(SEGMENT_MAP[value], MSBFIRST);
  }

  shift(SEGMENT_SELECT[segment], MSBFIRST);
  sbi(PORTD, LATCH_DIO);
}

void displayNumber(int countNum)
{
  // volatile int displayNumber = 0;

  if (countNum < 0 || countNum > 9999)
    return;

  if (countNum / 1000 > 0)
  {
    writeNumberToSegment(0, countNum / 1000);
  }
  if (countNum / 100 > 0)
  {
    writeNumberToSegment(1, (countNum / 100) % 10);
  }
  if (countNum / 10 > 0)
  {
    writeNumberToSegment(2, (countNum / 10) % 10);
  }
  writeNumberToSegment(3, countNum % 10);
}

void writeAnimation1()
{

  // uint8_t animation1[4][4] = {{0x88, 0x83, 0xC6, 0xA1}, {0x83, 0x83, 0xA1, 0xA1}, {0x88, 0x83, 0xC6, 0xA1}, {0x88, 0x88, 0xC6, 0x88}};
  uint8_t animation1[5][4] = {{0xff, 0x7f, 0xff, 0xff}, {0xff, 0x7b, 0xef, 0xff}, {0xff, 0x4b, 0xe9, 0xff}, {0x80, 0x4b, 0xe9, 0x80}, {0x00, 0x00, 0x00, 0x00}};

  int waitloop = 3000;

  // while (1)
  {
    BuzzerOn();
    for (int j = 0; j < 5; j++)
    {
      for (int timer = 0; timer < waitloop; timer++)
      {

        for (int i = 0; i < 4; i++)
        {
          writeValueToSegment(i, animation1[j][i]);
        }
      }
    }
  }
  BuzzerOff();
}

// Writes a symbol to a certain segment. Segment 0 is the leftmost.
void writeValueToSegment(uint8_t segment, uint8_t value)
{
  cbi(PORTD, LATCH_DIO);
  if (value == SPACE)
  {
    // Clear the segment
    shift(0xFF, MSBFIRST);
  }
  else
  {
    // Write the segment value
    shift(value, MSBFIRST);
  }

  shift(SEGMENT_SELECT[segment], MSBFIRST);
  sbi(PORTD, LATCH_DIO);
}
