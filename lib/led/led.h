#ifndef LED_H
#define LED_H

void enableLed(int);
void lightUp(int);
void lightDown(int);
void enableAllLeds();
void lightUpAll();
void lightDownAll();

#endif
