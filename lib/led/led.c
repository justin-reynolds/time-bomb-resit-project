#include "led.h"
#include <avr/io.h>

void enableLed(int led)
{
    DDRB |= (1 << (PB2 + led));
}

void lightUp(int led)
{
    PORTB &= ~(1 << (PB2 + led));
}

void lightDown(int led)
{
    PORTB |= (1 << (PB2 + led));
}

void enableAllLeds()
{
    DDRB |= (1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5);
}

void lightUpAll()
{
    PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5));
}

void lightDownAll()
{
    PORTB |= (1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5);
}