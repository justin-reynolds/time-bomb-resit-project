#include "arrays.h"

bool arraysAreIdentical(const int *arr1, const int *arr2, size_t size)
{
    for (size_t i = 0; i < size; i++)
    {
        if (*(arr1 + i) != *(arr2 + i))
        {
            return false;
        }
    }
    return true;
}

void resetArray(int *array, size_t size)
{
    for (size_t i = 0; i < size; i++)
    {
        *(array + i) = 0;
    }
}