#ifndef ARRAYS_H
#define ARRAYS_H

#include <stdbool.h>
#include <stdio.h>

bool arraysAreIdentical(const int *arr1, const int *arr2, size_t size);
void resetArray(int *array, size_t size);

#endif