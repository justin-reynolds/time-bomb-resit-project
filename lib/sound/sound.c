#define __DELAY_BACKWARD_COMPATIBLE__
#include "sound.h"

void soundBuzzerWithFrequency(uint16_t frequency, uint16_t duration_ms)
{

    if (!DISABLE_SOUND)
    {
        uint16_t halfPeriod_us = 1000000 / (2 * frequency);
        uint32_t numToggles = (uint32_t)1000 * duration_ms / (2 * halfPeriod_us);

        for (uint32_t i = 0; i < numToggles; i++)
        {
            PORTD ^= (1 << BUZZER_PIN); // Toggle the buzzer pin
            _delay_us(halfPeriod_us);
        }

        // Turn off the buzzer
        PORTD = (1 << BUZZER_PIN);
    }
}

void BuzzerOff()
{
    // Turn off the buzzer
    PORTD = (1 << BUZZER_PIN);
}

void BuzzerOn()
{
    // Turn off the buzzer
    PORTD = (0 << BUZZER_PIN);
}
