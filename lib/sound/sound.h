#ifndef SOUND_H
#define SOUND_H

#include <avr/io.h>
#include <util/delay.h>

#define BUZZER_PIN 3    // Example buzzer pin, change it to your actual pin
#define DISABLE_SOUND 0 // 1 will disable the sound output, 0 will enable outpt

void soundBuzzerWithFrequency(uint16_t frequency, uint16_t duration_ms);

void BuzzerOff();
void BuzzerOn();

#endif