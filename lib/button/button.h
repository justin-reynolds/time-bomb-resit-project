#ifndef button_h
#define button_h
#include <avr/io.h>

void initButton(uint8_t buttonpin);
int buttonsPressed(uint8_t buttonnum);

#endif

