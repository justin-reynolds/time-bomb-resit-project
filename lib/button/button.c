#include "button.h"

void initButton(uint8_t buttonpin)
{

    DDRC &= ~(1 << buttonpin); // Set pin as input
    PORTC |= (1 << buttonpin); // Enable pull-up resistor
}

int buttonsPressed(uint8_t buttonnum)
{
    return (PINC & (1 << buttonnum)) ? 0 : 1;
}