#include <usart.h>
#include <avr/io.h>

#include <avr/interrupt.h>
#include <button.h>
#include <led.h>
#include <sound.h>
#include <display.h>
#include <arrays.h>
#include <stdbool.h>

// define Pins for buttons (pull-up configuration) and buzzer
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define BUZZER_PIN 3

#define MINTIME 10 // minimum and maxim values that player can set for countdown timer
#define MAXTIME 240

// enum for game state
enum state
{
  settingup,
  countdown,
  win,
  lose
};

volatile uint32_t timer = 0; // Global variable for timer, must be volatile for use in interrupt function
unsigned long previousTime = 0;
const unsigned long Interval = 1000; // Interval for clock (based on 1000ms = 1 sec)
unsigned long previousButtonCheck = 0;
const unsigned long IntervalButtonCheck = 200; // Interval for clock (based on 1000ms = 1 sec)
unsigned long previousPotentioCheck = 0;
const unsigned long IntervalPotentioCheck = 200; // Interval for clock (based on 1000ms = 1 sec)

uint16_t potvalue = 0;     // new potentiometer value
uint16_t prevpotvalue = 0; // previous potentiometer value, for comparison code

int counter = 25; // startvalue will be 30 secs as first potentiometer value will cause 5 sec increase
// int counter = 10; // startvalue will be 60 secs , but for easy debugging we keep it low

int secret[3] = {3, 2, 1}; // array to store secret code
int entry[3] = {0, 0, 0};  // array to store code entry
int numberofpushes = 0;    // number of buttons that have been pressed
size_t size = sizeof(entry) / sizeof(entry[0]);

enum state gamestate;

void setup()
{

  // init usart
  initUSART();

  // initalize leds & display
  enableAllLeds();
  lightDownAll();
  initDisplay();

  // initialize buttons
  initButton(BUTTON1);
  initButton(BUTTON2);
  initButton(BUTTON3);

  // Set BUZZER_PIN as output
  DDRD |= (1 << BUZZER_PIN);
  // switch buzzer off
  PORTD = (1 << BUZZER_PIN);

  // DDRC &= ~_BV(DDC2);    /* PORTB4 as input */
  // PORTC |= _BV(PORTC2);  /* enable pull-up */
  // PCICR |= _BV(PCIE0);   /* enable Pin Change 0 interrupt */
  // PCMSK0 |= _BV(PCINT4); /* PORTB4 is also PCINT4 */

  // init ADC
  ADMUX |= (1 << REFS0);                                // Reference voltage 5V
  ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Division factor: 128
  ADCSRA |= (1 << ADEN);

  // Set up Timer1
  cli();      // Disable interrupts
  TCCR1A = 0; // Set Timer1 control registers to initial values
  TCCR1B = 0;
  TCNT1 = 0;                           // Set Timer1 counter value to 0
  OCR1A = 312;                         // Set Timer1 compare value (20ms  with 16 MHz clock, 15624 is 1 sec)
  TCCR1B |= (1 << WGM12);              // Configure Timer1 in CTC mode
  TCCR1B |= (1 << CS12) | (1 << CS10); // Set prescaler to 1024
  TIMSK1 |= (1 << OCIE1A);             // Enable Timer1 compare interrupt

  sei(); // Enable interrupts
}

int main(void)
{
  setup();

  gamestate = settingup;
  printString("Timebomb started! \n");

  while (1)
  {

    switch (gamestate)
    {
    case settingup:
      // display counter value on every loop iteration
      displayNumber(counter);

      // Read Potentiometer value on every loop iteration, unless the countdown has already started
      if (timer >= previousPotentioCheck + IntervalPotentioCheck)
      {
        previousPotentioCheck = timer;
        ADCSRA |= (1 << ADSC);                 // Start the analog --> digital conversion
        loop_until_bit_is_clear(ADCSRA, ADSC); // Wait until the conversion is completed
        potvalue = ADC;                        // Read the result
        if (potvalue > prevpotvalue + 1)       // add some extra margin to reduce potentiometer quirckiness
        {
          if (counter + 5 <= MAXTIME)
          {
            counter += 5;
          }

          prevpotvalue = potvalue;
        }
        if (potvalue < prevpotvalue - 1) // add some extra margin to reduce potentiometer quirckiness
        {
          if (counter - 5 >= MINTIME)
          {
            counter -= 5;
          }

          prevpotvalue = potvalue;
        }
      }

      // Check if a button has been pressed
      if (timer >= previousButtonCheck + IntervalButtonCheck)
      {
        previousButtonCheck = timer;

        if (buttonsPressed(BUTTON2))
        {
          printString("button2 pressed, starting countdown \n");
          gamestate = countdown;
        }
      }
      break;

    case countdown:
      displayNumber(counter);

      // Check if a second has passed
      if (timer >= previousTime + Interval)
      {
        // 1 second has passed
        previousTime = timer;
        counter -= 1;

        // check for defusing (win)

        // check for counter == 0, kaboom (lose)
        if (counter == 0)
        {
          gamestate = lose;
          displayNumber(counter);
        }
        // display counter value on every loop iteration

        // output remaining time to console
        printf("Remaining time: %d\n", counter);

        // checks for the last 5 seconds:
        switch (counter)
        {
        case 4:
          soundBuzzerWithFrequency(500, 50);
          break;

        case 3:
          soundBuzzerWithFrequency(1000, 50);
          lightUp(0);
          break;

        case 2:
          soundBuzzerWithFrequency(2000, 50);
          lightUp(1);
          break;

        case 1:
          soundBuzzerWithFrequency(4000, 50);
          lightUp(2);
          break;

        case 0:
          soundBuzzerWithFrequency(50, 50);
          lightUp(3);
          gamestate = lose;

          break;

        default:
          soundBuzzerWithFrequency(100, 100);
        }
      }

      // Check if a button has been pressed
      if (timer >= previousButtonCheck + IntervalButtonCheck)
      {
        previousButtonCheck = timer;

        if (buttonsPressed(BUTTON1))
        {
          printString("button1 pressed \n");
          entry[numberofpushes] = 1;
          numberofpushes++;
        }
        if (buttonsPressed(BUTTON2))
        {
          printString("button2 pressed \n");
          entry[numberofpushes] = 2;
          numberofpushes++;
        }
        if (buttonsPressed(BUTTON3))
        {
          printString("button3 pressed \n");
          entry[numberofpushes] = 3;
          numberofpushes++;
        }

        if (numberofpushes == 3)
        {
          // entire code was entered; check if it matches the secret code
          // here we pass the array by reference, because we reset all values to zero in the function which would not work when passing by value
          if (arraysAreIdentical(entry, secret, size))
          {
            printf("The code is broken!!!\n");
            gamestate = win;
          }
          else
          {
            printf("code is incorrect\n");
            resetArray(entry, size);
            numberofpushes = 0;
          }
        }
      }

      break;

    case win:
      // the correct code was entered, you won
      printString("Bomb defused, good work ! \n");

      break;

    case lose:

      // bomb exploded, you lost
      printString("The bomb exploded !!! \n");

      writeAnimation1();

      break;
    }
  }

  return 0;
}

// Interrupt Routine for Timer
ISR(TIMER1_COMPA_vect)
{
  // runs every 20 ms, set clock
  timer += 20; // Increment 10th seconds count
}
